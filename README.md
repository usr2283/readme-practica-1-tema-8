# TAREA 4 : Gestión de ramas
## Secciones

- [Ejercicio 1](#Ejercicio-1)
- [Ejercicio 2](#Ejercicio-2)
- [Ejercicio 3](#Ejercicio-3)
- [Ejercicio 4](#Ejercicio-4)
- [Ejercicio 5](#Ejercicio-5)

---

## Ejercicio 1:

Creamos la nueva rama bibliogradia con el comando **git branch _(nombre de la nueva rama)_** y con el comando **git branch -av** veremos las ramas del repositorio juento el sha del commit.

![Imagen_1](./Images/Imagen1.png "Imagen1") 

---

## Ejercicio 2:

Crearemos una pagina web llamada capitulo4.html que tendra el siguiente contenido:

![Imagen_2](./Images/Imagen2.png "Imagen2") 

Despues añadimos los cambios al Stating Area con el comando **git add _(Nombre del archivo o carpeta)_**. Realizaremos el commit con el comando **git commit -m _(Descripción del commit)_** y terminaremos mostrando el historial del repositorio (incluyendo todas las ramas, de forma gráfica) con el comando **git log --graph --all --online**

![Imagen_3](./Images/Imagen3.png "Imagen3")

---

## Ejercicio 3:

Cambiaremos a la rama bibliografia con el comando **git checkout _(Nombre de la rama)_** y crearemos la página web bibliografia.

![Imagen_4](./Images/Imagen4.png "Imagen4")

Dejaremos el archivo .html de la siguiente manera:

![Imagen_5](./Images/Imagen5.png "Imagen5")

Añadimos los cambios al Stating Area con el comando **git add _(Nombre del archivo o carpeta)_**. Realizaremos el commit con el comando **git commit -m _(Descripción del commit)_** y terminaremos mostrando el historial del repositorio (incluyendo todas las ramas, de forma gráfica) con el comando **git log --graph --all --online**

![Imagen_6](./Images/Imagen6.png "Imagen6")

---

## Ejercicio 4:

Para fusionar la rama bibliografia con la rama master, tendremos que volver a la rama master con el comando **git checkout _(Nombre de la rama)_**, fusionaremos las ramas con el comando **git megre _(Nombre de la rama)_** y mostramos el historial de repositorio (incluyendo todas las ramas) con el comando **git log --graph --all --oneline**:

![Imagen_7](./Images/Imagen7.png "Imagen7")

Eliminamos la rama bibliografia con el comando **git branch -d _(Nombre de la rama)_** y volvemos a mostrar el historial de repositotio:

![Imagen_8](./Images/Imagen8.png "Imagen8")

---

## Ejercicio 5:

Crearemos la rama bibliografia con el comando **git branch _(Nombre de la rama)_** y cambiamos de rama con el comando **git checkout _(Nombre de la rama)_**

![Imagen_9](./Images/Imagen9.png "Imagen9")

Creamos el fichero bibliografia.html y lo dejaremos de la siguiente manera:

![Imagen_10](./Images/Imagen10.png "Imagen10")

Añadimos los cambios al String Area con el comando **git add _Nombre archivo o carpeta_** y hacemos un commit con **git commit -m "Descripción del commit"**

![Imagen_11](./Images/Imagen11.png "Imagen11")

Cambiamos a la rama master con **git checkout _(Nombre de la rama)_**

![Imagen_12](./Images/Imagen12.png "Imagen12")

Volvemos a cambiar el fichero bibliografia.html:

![Imagen_13](./Images/Imagen13.png "Imagen13")

Añadimos los cambios al String Area con el comando **git add _Nombre archivo o carpeta_** y hacemos un commit con **git commit -m "Descripción del commit"**

![Imagen_14](./Images/Imagen14.png "Imagen14")

Fusionamos la rama bibliografia con la master con el comando **git merge _(Nombre de la rama)_**

![Imagen_15](./Images/Imagen15.png "Imagen15")

Cambiamos el archivo html de nuevo para resolver el conflicto:

![Imagen_16](./Images/Imagen16.png "Imagen16")

Para terminar esta tarea añadiremos los cambios al String Area con el comando **git add _Nombre archivo o carpeta_**, haremos un commit con **git commit -m "Descripción del commit"** y mostraremos el historial del repositorio (incluyendo las ramas).

![Imagen_17](./Images/Imagen17.png "Imagen17")